import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main
{
    public static void main(String[] args) throws Exception
    {
        Scanner scanner = new Scanner(System.in);

        int T = scanner.nextInt();

        for (int i = 0; i < T; i++)
        {
            int N = scanner.nextInt();
            int M = scanner.nextInt();
            int K = scanner.nextInt();
            int[][] table = new int[N][N];
            for (int j = 0; j < M; j++)
            {
                int A = scanner.nextInt() - 1;
                int B = scanner.nextInt() - 1;
                int G = scanner.nextInt();
                if (table[A][B] == 0 || G < table[A][B])
                {
                    table[A][B] = table[B][A] = G;
                }
            }
            int[][] families = new int[K][2];
            for (int j = 0; j < K; j++)
            {
                int S = scanner.nextInt() - 1;
                int D = scanner.nextInt() - 1;
                families[j][0] = S;
                families[j][1] = D;
            }
            int[][] mincost = new int[N][];
            boolean[] flag = new boolean[N];
            for (int p1 = 0; p1 < N; p1++)
            {
                int[] cost = new int[N];
                Arrays.fill(cost, Integer.MAX_VALUE);
                Arrays.fill(flag, false);
                cost[p1] = 0;
                for (int k = 0; k < N; k++)
                {
                    int mi = -1;
                    for (int j = 0; j < N; j++)
                    {
                        if (flag[j])
                        {
                            continue;
                        }
                        if (mi < 0 || cost[j] < cost[mi])
                        {
                            mi = j;
                        }
                    }
                    if (mi < 0)
                    {
                        break;
                    }
                    if (cost[mi] == Integer.MAX_VALUE)
                    {
                        break;
                    }
                    flag[mi] = true;
                    for (int j = 0; j < N; j++)
                    {
                        if (table[mi][j] == 0)
                        {
                            continue;
                        }
                        int tmp = cost[mi] + table[mi][j];
                        if (tmp < cost[j])
                        {
                            cost[j] = tmp;
                        }
                    }
                }
                mincost[p1] = cost;
            }
            Map<Integer, Integer> map = new HashMap<>();
            Set<Integer> list = new HashSet<>(), tmp = new HashSet<>(), swap;
            {
                int findex = 0;
                int current = families[findex][0];
                int total = mincost[0][current];
                int belongings = 1;
                Integer key = Integer.valueOf((findex << 9) | (current << 2) | belongings);
                map.put(key, Integer.valueOf(total));
                list.add(key);
            }
            int answer = -1;
            while (!list.isEmpty())
            {
                tmp.clear();
                for (Integer key : list)
                {
                    int findex = key.intValue() >> 9;
                    int current = (key.intValue() >> 2) & 0x7f;
                    int belongings = key.intValue() & 0x3;
                    int total = map.get(key).intValue();
                    switch (belongings)
                    {
                    case 0:
                        {
                            int loadTo = families[findex][0];
                            if (mincost[current][loadTo] == Integer.MAX_VALUE)
                            {
                                break;
                            }
                            total += mincost[current][loadTo];
                            key = Integer.valueOf((findex << 9) | (loadTo << 2) | 1);
                            if (map.containsKey(key) && map.get(key).intValue() < total)
                            {
                                break;
                            }
                            map.put(key, Integer.valueOf(total));
                            tmp.add(key);
                        }
                        break;
                    case 1:
                        if (findex + 1 < K)
                        {
                            int loadTo = families[findex + 1][0];
                            if (mincost[current][loadTo] < Integer.MAX_VALUE)
                            {
                                int total2 = total + mincost[current][loadTo];
                                Integer key2 = Integer.valueOf((findex << 9) | (loadTo << 2) | 2);
                                if (!map.containsKey(key2) || total2 < map.get(key2).intValue())
                                {
                                    map.put(key2, Integer.valueOf(total2));
                                    tmp.add(key2);
                                }
                            }
                        }
                    case 2:
                        {
                            int deliverTo = families[findex][1];
                            if (mincost[current][deliverTo] == Integer.MAX_VALUE)
                            {
                                break;
                            }
                            total += mincost[current][deliverTo];
                            belongings--;
                            findex++;
                            if (findex == K)
                            {
                                if (answer < 0 || total < answer)
                                {
                                    answer = total;
                                }
                                break;
                            }
                            key = Integer.valueOf((findex << 9) | (deliverTo << 2) | belongings);
                            if (map.containsKey(key) && map.get(key).intValue() < total)
                            {
                                break;
                            }
                            map.put(key, Integer.valueOf(total));
                            tmp.add(key);
                        }
                        break;
                    }
                }
                swap = tmp;
                tmp = list;
                list = swap;
            }
            System.out.printf("Case #%d: %d\n", i + 1, answer);
        }

    }
}