import java.util.Scanner;

public class Main
{
    static final String WHITE = "white", BLACK = "black";
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
    
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        
        int T = scanner.nextInt();
        
        for (int i = 0; i < T; i++)
        {
            double P = scanner.nextDouble();
            double X = scanner.nextDouble();
            double Y = scanner.nextDouble();
            
            // System.err.printf("P:%f, X:%f, Y:%f\n", P, X, Y);
            
            if (P == 0.0)
            {
                output(i, WHITE);
                continue;
            }
            
            double dx = X - 50.0;
            double dy = Y - 50.0;
            double dist = Math.sqrt(dx * dx + dy * dy);
            
            // System.err.printf("dx:%f, dy:%f, dist:%f\n", dx, dy, dist);
            
            if (dist > 50.0) {
                output(i, WHITE);
                continue;
            }
            
            if (P == 100.0)
            {
                output(i, BLACK);
                continue;
            }
            
            if (dy == 0.0)
            {
                if (dx < 0.0)
                {
                    output(i, P < 75.0 ? WHITE : BLACK);
                }
                else if (dx > 0.0)
                {
                    output(i, P < 25.0 ? WHITE : BLACK);
                }
                else
                {
                    output(i, BLACK);
                }
                continue;
            }
            
            double q = 0.0;
            
            if (dy >= 0.0 && dx >= 0.0)
            {
                q = Math.atan(dx / dy) * 50.0 / Math.PI;
            }
            else if (dy >= 0.0 && dx < 0.0)
            {
                q = Math.atan(-dx / dy) * 50.0 / Math.PI + 75.0;
            }
            else if (dy < 0.0 && dx >= 0.0)
            {
                q = Math.atan(-dx / dy) * 50.0 / Math.PI + 25.0;
            }
            else if (dy < 0.0 && dx < 0.0)
            {
                q = Math.atan(dx / dy) * 50.0 / Math.PI + 50.0;
            }
            
            // System.err.printf("q:%f\n", q);
            
            output(i, q > P ? WHITE : BLACK);
        }
    }
}