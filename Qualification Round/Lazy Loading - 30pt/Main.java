import java.util.Scanner;
import java.util.Arrays;

public class Main
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        
        int T = scanner.nextInt();
        
        for (int i = 0; i < T; i++)
        {
            int N = scanner.nextInt();
            int[] Ws = new int[N];
            for (int j = 0; j < N; j++)
            {
                Ws[j] = scanner.nextInt();
            }
            Arrays.sort(Ws);
            int answer = 0;
            int head = 0, tail = N - 1;
            while (head <= tail)
            {
                if (Ws[tail] >= 50)
                {
                    answer++;
                    tail--;
                    continue;
                }
                int needItems = 50 / Ws[tail] + (50 % Ws[tail] > 0 ? 1 : 0) - 1;
                if (head + needItems <= tail)
                {
                    answer++;
                    head +=needItems;
                    tail--;
                    continue;
                }
                break;
            }
            System.out.printf("Case #%d: %d\n", i + 1, answer);
        }
    }
}


