import java.util.Scanner;
import java.util.regex.*;

public class Main
{
    public static void main(String[] args)
    {
        Pattern pattern = Pattern.compile("^(\\d+)d(\\d+)(\\D\\d+)?$");
        int[] dices = {4, 6, 8, 10, 12, 20};
        double[][][] table = new double[21][21][401];
        for (int dice : dices) {
            double[][] temp = new double[21][20 * dice + 1];
            for (int j = 1; j <= dice; j++)
            {
                temp[1][j] = 1.0;
            }
            for (int k = 1; k < 20; k++)
            {
                double[] to = temp[k + 1], from = temp[k];
                int end = k * dice;
                for (int j = 1; j <= end; j++)
                {
                    for (int d = 1; d <= dice; d++)
                    {
                        to[j + d] += from[j];
                    }
                }
            }
            for (int k = 1; k <= 20; k++)
            {
                double[] ktemp = temp[k];
                int end = k * dice;
                for (int j = end; 1 < j; j--)
                {
                    ktemp[j - 1] += ktemp[j];
                }
                double[] ktable = table[dice][k];
                for (int j = 1; j <= end; j++)
                {
                    ktable[j] = ktemp[j] / ktemp[1];
                }
            }
        }
        Scanner scanner = new Scanner(System.in);
        int T = scanner.nextInt();        
        for (int i = 0; i < T; i++)
        {
            int H = scanner.nextInt();
            int S = scanner.nextInt();
            double answer = 0.0;
            for (int j = 0; j < S; j++)
            {
                String spell = scanner.next();
                Matcher matcher = pattern.matcher(spell);
                matcher.matches();
                int X = Integer.parseInt(matcher.group(1));
                int Y = Integer.parseInt(matcher.group(2));
                int Z = 0;
                if (matcher.group(3) != null && matcher.group(3).length() > 0)
                {
                    Z = Integer.parseInt(matcher.group(3));
                }
                int rem = H - Z;
                if (rem <= X)
                {
                    answer = 1.0;
                }
                else if (rem <= X * Y)
                {
                    answer = Math.max(answer, table[Y][X][rem]);
                }
            }
            System.out.printf("Case #%d: %.6f\n", i + 1, answer);
        }
    }
}